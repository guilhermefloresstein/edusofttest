package com.edusoft.teste.service;

import com.edusoft.teste.model.Student;
import com.edusoft.teste.model.estrategy.FrequencyFactory;
import com.edusoft.teste.model.estrategy.FrequencyStrategy;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class FrequencyService {

    public Collection<Student> getFrequency(Collection<Student> students) {

        FrequencyStrategy strategy = FrequencyFactory.getFrequencyStrategy();

        students.forEach(student -> student.setFrequency(strategy.calculateFrequency(student)));

        return students;
    }

}
