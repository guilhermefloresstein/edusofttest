package com.edusoft.teste.service;

import com.edusoft.teste.model.Student;
import com.edusoft.teste.model.estrategy.AverageFactory;
import com.edusoft.teste.model.estrategy.AverageStrategy;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CalculateAverageService {

    public Collection<Student> getAverage(Collection<Student> students) {
        AverageStrategy averageStrategy = AverageFactory.getAverageStrategy();

        students.forEach(student -> student.setAverage(averageStrategy.calculate(student)));

        return students;
    }
}
