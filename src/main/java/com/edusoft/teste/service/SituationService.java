package com.edusoft.teste.service;

import com.edusoft.teste.model.Parameters;
import com.edusoft.teste.model.Student;
import com.edusoft.teste.model.StudentResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
@RequiredArgsConstructor
public class SituationService {

    private final CalculateAverageService averageService;
    private final FrequencyService frequencyService;
    private Parameters params;

    public Collection<StudentResult> getSituations(Collection<Student> students) {
        Collection<StudentResult> results = new ArrayList<>();

        averageService.getAverage(students);
        frequencyService.getFrequency(students);

        params = Parameters.getParameters();

        students.forEach(student -> {
            StudentResult result = new StudentResult();
            result.setAverage(student.getAverage());
            result.setIdStudent(student.getId());
            setStatus(student, result);

            // guarda o status no student para o relatorio
            student.setStatus(result.getStatus());
            results.add(result);
        });

        return results;
    }

    private void setStatus(Student student, StudentResult result) {
        if (student.getFrequency().compareTo(params.getMinFrequency()) < 0) {
            result.setStatus(StudentResult.Status.RF);
            return;
        }

        if (student.getAverage().compareTo(params.getMinAverage()) < 0) {
            result.setStatus(StudentResult.Status.RM);
            return;
        }

        result.setStatus(StudentResult.Status.AP);
    }

}
