package com.edusoft.teste.service;

import com.edusoft.teste.model.ReturnDto;
import com.edusoft.teste.model.Student;
import com.edusoft.teste.model.StudentResultDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class StudentsService {

    private final RestTemplate template = new RestTemplate();

    @Value("${url.recuperaAlunos.token}")
    private String URL_TOKEN;

    @Value("${url.recuperaAlunos.execute}")
    private String URL_STUDENTS;

    @Value("${url.gravaResultado.token}")
    private String URL_SEND_TOKEN;

    @Value("${url.gravaResultado.execute}")
    private String URL_SEND;

    private String getToken(String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("usuario", "mentor");
        headers.add("senha", "123456");
        HttpEntity<String> request = new HttpEntity<>(headers);

        ResponseEntity<String> entity = template.exchange(url, HttpMethod.GET, request, String.class);
        if (entity.getStatusCode().equals(HttpStatus.OK)) {
            return entity.getBody();
        }

        return null;
    }

    public Collection<Student> getStudents() {
        String token = getToken(URL_TOKEN);
        if (token != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("token", token);
            headers.add("content-type", MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<String> request = new HttpEntity<>("", headers);
            ResponseEntity<ReturnDto[]> entity = template.postForEntity(URL_STUDENTS, request, ReturnDto[].class);

            if (entity.getStatusCode().equals(HttpStatus.OK) && entity.getBody() != null) {
                //TODO: Verificar se sempre tera apenas um unico item na resposta
                return entity.getBody()[0].getStudents();
            }
        }

        return new ArrayList<>();
    }

    public boolean sendResults(StudentResultDto dto) {
        String token = getToken(URL_SEND_TOKEN);
        if (token != null) {
            HttpHeaders headers = new HttpHeaders();
            headers.add("token", token);
            headers.add("content-type", MediaType.APPLICATION_JSON_VALUE);
            HttpEntity<StudentResultDto> request = new HttpEntity<>(dto, headers);
            ResponseEntity<String> entity = template.postForEntity(URL_SEND, request, String.class);

            return entity.getStatusCode().equals(HttpStatus.OK) && entity.getBody() != null;
        }

        return false;
    }

}
