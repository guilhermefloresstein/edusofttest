package com.edusoft.teste.controller;

import com.edusoft.teste.model.Student;
import com.edusoft.teste.model.StudentResult;
import com.edusoft.teste.model.StudentResultDto;
import com.edusoft.teste.service.SituationService;
import com.edusoft.teste.service.StudentsService;
import com.edusoft.teste.util.GeneratePdfReport;
import com.itextpdf.text.DocumentException;
import lombok.AllArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.ByteArrayInputStream;
import java.util.Collection;

@RestController
@AllArgsConstructor
public class StudentController {

    private final StudentsService studentsService;
    private final SituationService situationService;

    @GetMapping(value = "/students/situation", produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> getStudentsSituation() throws DocumentException {
        Collection<StudentResult> results;

        Collection<Student> students = studentsService.getStudents();
        if (students.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        results = situationService.getSituations(students);

        StudentResultDto dto = new StudentResultDto();
        dto.addStudents(results);

        studentsService.sendResults(dto);

        ByteArrayInputStream bis = GeneratePdfReport.studentsReport(students);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=students.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

}
