package com.edusoft.teste.util;

import com.edusoft.teste.model.Student;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicInteger;

public class GeneratePdfReport {

    // TODO: PDF não esta dinamico...
    public static ByteArrayInputStream studentsReport(Collection<Student> students) throws DocumentException {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        PdfPTable table = new PdfPTable(7);
        table.setWidthPercentage(90);
        table.setWidths(new int[]{2,1,1,1,1,1,2});

        table.addCell(createHeaderCell("Nome"));
        table.addCell(createHeaderCell("Nota 1"));
        table.addCell(createHeaderCell("Nota 2"));
        table.addCell(createHeaderCell("Nota 3"));
        table.addCell(createHeaderCell("Faltas"));
        table.addCell(createHeaderCell("Média"));
        table.addCell(createHeaderCell("Resultado"));

        students.forEach(student -> {
            table.addCell(createBodyCell(student.getName()));
            AtomicInteger faltas = new AtomicInteger(0);
            student.getNotes().forEach(grade -> {
                table.addCell(createBodyCell(grade.getNote().toString()));
                faltas.getAndAccumulate(grade.getFault(), Integer::sum);
            });
            table.addCell(createBodyCell(String.valueOf(faltas.get())));
            table.addCell(createBodyCell(student.getAverage().toString()));
            table.addCell(createBodyCell(student.getStatus().toString()));
        });

        PdfWriter.getInstance(document, out);
        document.open();
        document.add(table);

        document.close();

        return new ByteArrayInputStream(out.toByteArray());
    }

    private static PdfPCell createHeaderCell(String text) {
        PdfPCell hcell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA_BOLD)));
        hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
        return hcell;
    }

    private static PdfPCell createBodyCell(String text) {
        PdfPCell cell = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA)));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setVerticalAlignment(Element.ALIGN_CENTER);
        return cell;
    }

}
