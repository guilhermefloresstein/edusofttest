package com.edusoft.teste.model.estrategy;

import com.edusoft.teste.model.Grade;
import com.edusoft.teste.model.Student;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicInteger;

@Data
public class ArithmeticAverageStrategy implements AverageStrategy {
    @Override
    public BigDecimal calculate(Student student) {

        BigDecimal sun = new BigDecimal(0);
        AtomicInteger count = new AtomicInteger(0);

        for (Grade note : student.getNotes()) {
            sun = sun.add(note.getNote());
            count.incrementAndGet();
        }

        return sun.divide(new BigDecimal(count.get()), RoundingMode.HALF_UP);
    }
}
