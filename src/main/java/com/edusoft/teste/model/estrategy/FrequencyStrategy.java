package com.edusoft.teste.model.estrategy;

import com.edusoft.teste.model.Student;

import java.math.BigDecimal;

public interface FrequencyStrategy {

    BigDecimal calculateFrequency(Student student);

}
