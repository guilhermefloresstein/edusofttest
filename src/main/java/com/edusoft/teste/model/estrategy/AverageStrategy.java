package com.edusoft.teste.model.estrategy;

import com.edusoft.teste.model.Student;

import java.math.BigDecimal;

public interface AverageStrategy {

    BigDecimal calculate(Student student);

}
