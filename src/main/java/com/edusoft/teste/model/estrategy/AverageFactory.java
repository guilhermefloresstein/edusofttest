package com.edusoft.teste.model.estrategy;

public class AverageFactory {

    // definir a estrategia para escolha e implementar
    public static AverageStrategy getAverageStrategy(){
        return new ArithmeticAverageStrategy();
    }

}
