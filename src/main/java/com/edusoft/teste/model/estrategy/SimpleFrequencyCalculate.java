package com.edusoft.teste.model.estrategy;


import com.edusoft.teste.model.Student;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicReference;

public class SimpleFrequencyCalculate implements FrequencyStrategy {

    @Override
    public BigDecimal calculateFrequency(Student student) {

        AtomicReference<Integer> faults = new AtomicReference<>(0);
        student.getNotes().forEach(grade -> {
            faults.updateAndGet(v -> v + grade.getFault());
        });


        return BigDecimal.valueOf(100f - ((100f * faults.get()) / student.getTotalLesson())).setScale(2, RoundingMode.HALF_UP);
    }
}
