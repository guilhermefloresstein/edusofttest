package com.edusoft.teste.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Collection;

@Data
public class Student {

    @JsonProperty("COD")
    private Long id;

    @JsonProperty("NOME")
    private String name;

    @JsonProperty("TOAL_AULAS")
    private Integer totalLesson;

    @JsonProperty("nota")
    private Collection<Grade> notes;

    @JsonIgnore
    private BigDecimal average;

    @JsonIgnore
    private BigDecimal frequency;

    @JsonIgnore
    private StudentResult.Status status;

}
