package com.edusoft.teste.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.Collection;

@Data
public class StudentResultDto {

    @JsonProperty("resultadoAluno")
    Collection<StudentResult> students;

    public void addStudents(Collection<StudentResult> sr) {
        if (students == null) {
            students = new ArrayList<>();
        }
        students.addAll(sr);
    }

}
