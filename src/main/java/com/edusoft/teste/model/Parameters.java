package com.edusoft.teste.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Parameters {

    private BigDecimal minAverage;
    private BigDecimal minFrequency;

    // Deve vir de banco, com diferentes parametros, para cada curso ou turma...
    public static Parameters getParameters() {
        Parameters param = new Parameters();
        param.setMinAverage(BigDecimal.valueOf(7));
        param.setMinFrequency(BigDecimal.valueOf(70));

        return param;
    }

}
