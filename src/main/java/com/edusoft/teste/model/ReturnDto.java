package com.edusoft.teste.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Collection;

@Data
public class ReturnDto {

    @JsonProperty("resultado")
    private String result;

    @JsonProperty("alunos")
    private Collection<Student> students;

}
