package com.edusoft.teste.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class Grade {

    @JsonProperty("NOTA")
    private BigDecimal note;

    @JsonProperty("FALTAS")
    private Integer fault;

}
