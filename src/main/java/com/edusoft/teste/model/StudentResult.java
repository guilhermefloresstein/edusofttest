package com.edusoft.teste.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class StudentResult {

    @JsonProperty("MEDIA")
    private BigDecimal average;

    @JsonProperty("RESULTADO")
    private Status status;

    @JsonProperty("COD_ALUNO")
    private Long idStudent;

    @JsonProperty("SEU_NOME")
    private String myName = "Guilherme Flores Stein";

    // deve ser uma tabela...
    public enum Status {
        AP, RM, RF
    }
}
