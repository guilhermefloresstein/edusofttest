## Relat�rio de execu��o do projeto

Tempo gasto em horas: **6hrs**

Dificuldades encontradas:
*   Entendimento e convers�o do modelo de dados.
*   Exagerei na separa��o dos servi�os para fazer os calculos, 
deveria ter ficado mais simples, 
o que atrasou o desenvolvimento.

Descri��o do projeto:

        Utilisei o spring boot para o desenvolvimento, criei uma camada web para facilitar
    a utiliza��o, basta chamar no navegador localhost:8080/students/situation e o sistema 
    vai buscar os alunos, calcular as medias, enviar os resultados e retornar um relat�rio.
        Fiz o relat�rio da maneira mais simples poss�vel, est� tudo fixo, pois n�o tinha 
    muita informa��o de como implementar o mesmo.
        Utilizei o Intellij IDEA para o desenvolvimento da aplica��o e jdk 1.8
